(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["scokit"] = factory();
	else
		root["scokit"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const scotkit = {\r\n  loader: {\r\n    before() { console.log(`Show loading`); },\r\n    after(result) { console.log(`Close loading...`); console.log(result); },\r\n    error(error) { console.log(`Show error !`); console.log(error); },\r\n    promises:[],\r\n    addPromises(...promises) {\r\n      this.promises = this.promises.concat(promises);\r\n      return this;\r\n    },\r\n    async monitor() {\r\n      this.before();\r\n      await Promise.all(this.promises)\r\n        .then(result => { this.after(result); })\r\n        .catch(error => { this.after(); this.error(error); });\r\n      this.promises = [];\r\n    },\r\n  },\r\n  datatable: async (tableID, navID, axiosInstance, url, callbackTh, callbackTd, navRender = true) => {\r\n    // reset content\r\n    let table = document.getElementById(tableID);\r\n    let nav = document.getElementById(navID);\r\n    table.innerHTML = '';\r\n    if (navRender) nav.innerHTML = '';\r\n\r\n    let thead = document.createElement(\"thead\"); \r\n    let tr = document.createElement(\"tr\"); \r\n    thead.appendChild(tr);\r\n\r\n    for (let title of callbackTh()) {\r\n      let th = document.createElement(\"th\"); \r\n      th.innerHTML = title;\r\n      tr.appendChild(th);\r\n    }\r\n    table.appendChild(thead);\r\n\r\n    await axiosInstance.get(url)\r\n      .then(response => {\r\n        let tbody = document.createElement(\"tbody\"); \r\n        table.appendChild(tbody);\r\n\r\n        for (let data of response.data.data) {\r\n          let tr = document.createElement(\"tr\"); \r\n          for (let d of callbackTd(data)) {\r\n            let td = document.createElement(\"td\"); \r\n            td.innerHTML = d;\r\n            tr.appendChild(td);\r\n          }\r\n          tbody.appendChild(tr);\r\n        }\r\n        \r\n        nav.innerHTML = response.data.links;\r\n\r\n        let aElements = nav.getElementsByTagName('a');\r\n        for (let i = 0; i < aElements.length; i++) {\r\n          let element = aElements[i];\r\n          let href = element.attributes.href.value;\r\n          element.addEventListener('click', function(event) {\r\n            scotkit.loader.addPromises(\r\n              scotkit.datatable(tableID, navID, axiosInstance, href, callbackTh, callbackTd, false)\r\n            ).monitor();\r\n            event.preventDefault();\r\n          });\r\n        }\r\n      })\r\n      .catch(error => {\r\n        throw error;\r\n      });\r\n  }\r\n}\r\n\r\nmodule.exports = scotkit;\r\n\n\n//# sourceURL=webpack://scokit/./index.js?");

/***/ })

/******/ });
});