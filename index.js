const scotkit = {
  loader: {
    before() { console.log(`Show loading`); },
    after(result) { console.log(`Close loading...`); console.log(result); },
    error(error) { console.log(`Show error !`); console.log(error); },
    promises:[],
    addPromises(...promises) {
      this.promises = this.promises.concat(promises);
      return this;
    },
    async monitor() {
      this.before();
      await Promise.all(this.promises)
        .then(result => { this.after(result); })
        .catch(error => { this.after(); this.error(error); });
      this.promises = [];
    },
  },
  datatable: async (tableID, navID, axiosInstance, url, callbackTh, callbackTd, navRender = true) => {
    // reset content
    let table = document.getElementById(tableID);
    let nav = document.getElementById(navID);
    table.innerHTML = '';
    if (navRender) nav.innerHTML = '';

    let thead = document.createElement("thead"); 
    let tr = document.createElement("tr"); 
    thead.appendChild(tr);

    for (let title of callbackTh()) {
      let th = document.createElement("th"); 
      th.innerHTML = title;
      tr.appendChild(th);
    }
    table.appendChild(thead);

    await axiosInstance.get(url)
      .then(response => {
        let tbody = document.createElement("tbody"); 
        table.appendChild(tbody);

        for (let data of response.data.data) {
          let tr = document.createElement("tr"); 
          for (let d of callbackTd(data)) {
            let td = document.createElement("td"); 
            td.innerHTML = d;
            tr.appendChild(td);
          }
          tbody.appendChild(tr);
        }
        
        nav.innerHTML = response.data.links;

        let aElements = nav.getElementsByTagName('a');
        for (let i = 0; i < aElements.length; i++) {
          let element = aElements[i];
          let href = element.attributes.href.value;
          element.addEventListener('click', function(event) {
            scotkit.loader.addPromises(
              scotkit.datatable(tableID, navID, axiosInstance, href, callbackTh, callbackTd, false)
            ).monitor();
            event.preventDefault();
          });
        }
      })
      .catch(error => {
        throw error;
      });
  }
}

module.exports = scotkit;
