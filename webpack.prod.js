const path = require('path');

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  entry: './index.js',
  output: {
    filename: 'scokit.min.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'scokit',
    libraryTarget: 'umd'
  }
};
