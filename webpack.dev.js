const path = require('path');

module.exports = {
  mode: 'development',
  entry: './index.js',
  output: {
    filename: 'scokit.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'scokit',
    libraryTarget: 'umd'
  }
};
