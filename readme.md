
# Scokit
Here are some cool toolkits for you.

## Installing

Using npm:

```bash
$ npm install scokit
```

Using cdn:
```
<script src="https://unpkg.com/scokit/dist/scokit.min.js"></script>
```

# Features
  - `datatable` : implement laravel paginator with restful api ... [[demo]](https://scottchayaa-npm.gitlab.io/scokit/) 